import json
import re
import jalali


class ConvertPersianToGregorian:

    @staticmethod
    def is_timestamp(field_name: str):
        pattern_timestamp = re.compile("^\\d{4}[-]?\\d{1,2}[-]?\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,3}[.]?[0-9]+$")
        if pattern_timestamp.match(str(field_name)):
            return True
        return False

    def convert_persian_to_gregorian(self, file_name: str, model_name_list: list, field_list: list):
        with open(str(file_name), "r") as json_file:
            json_file = json.load(json_file)
            for key, data in enumerate(json_file, 0):
                for model_name in model_name_list:
                    if data["model"] == str(model_name):
                        for field in field_list:
                            fields = data["fields"]
                            if fields.get(str(field)):
                                timestamp = (data["fields"][str(field)]).replace("T", " ").strip()
                                result_is_timestamp = self.is_timestamp(timestamp)
                                if result_is_timestamp:
                                    print("\nkey:", key, ", model:", data["model"], ", field:", field, ", value:",
                                          json_file[key]["fields"][str(field)])

                                    date = timestamp[:10]
                                    time = timestamp[11:19]


                                    try:
                                        date_new = jalali.Persian(date).gregorian_string()
                                        timestamp_new = date_new + " " + time
                                        with open("backup.json", "w") as current_file:
                                            json_file[key]["fields"][str(field)] = timestamp_new
                                            json.dump(json_file, current_file)
                                            print("Success : Field with name <{}> convert.".format(field))
                                    except:
                                        print("Error : Field with name <{}> is gregorian.".format(field))
                                    print(
                                        "\n------------------------------------ end field <{}> ------------------------------------".format(
                                            field))


field_list = ["timestamp", "created_at", "time"]
model_name_list = ["orders.order", "products.product"]

obj = ConvertPersianToGregorian()
obj.convert_persian_to_gregorian("backup.json", model_name_list, field_list)
